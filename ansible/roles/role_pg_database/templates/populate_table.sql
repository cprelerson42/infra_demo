DROP TABLE IF EXISTS "{{ psql_table_name }}";

CREATE TABLE "{{ psql_table_name }}"(
    entry_id serial PRIMARY KEY,
    username VARCHAR (50) UNIQUE NOT NULL,
    pass VARCHAR (50) NOT NULL,
    secret_number INT NOT NULL
);

INSERT INTO "{{ psql_table_name }}" (username, pass, secret_number)
VALUES(
    {% for value in psql_table_values %}
    '{{ value }}',
    {%endfor%}
    {{ 1000 | random }}
)
;