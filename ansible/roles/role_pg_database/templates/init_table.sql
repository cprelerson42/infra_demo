DROP TABLE IF EXISTS "{{ psql_table_name }}";

CREATE TABLE "{{ psql_table_name }}"();
	
ALTER TABLE "{{ psql_table_name }}" OWNER TO web_user;
