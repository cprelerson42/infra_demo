provider "google" {
    credentials = file("gcp_creds.json")
    project = "infra-demo-265315"
    region = "us-central1"
    zone = "us-central1-a"
}

# https://www.terraform.io/docs/providers/google/r/container_cluster.html
resource "google_container_cluster" "primary" {
  name               = "tf-kube-cluster"
  location           = "us-central1-a"
  initial_node_count = 3

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    # service_account = "kube-account"

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = {
      foo = "bar"
    }

    tags = ["foo", "bar"]
  }

  timeouts {
    create = "30m"
    update = "40m"
  }

  provisioner "local-exec"{
    command = "gcloud container clusters get-credentials tf-kube-cluster --zone us-central1-a --project infra-demo-265315"
  }
}