# Create the various security groups for our server and database servers
# https://docs.aws.amazon.com/vpc/latest/userguide/getting-started-ipv4.html#getting-started-create-security-group

# Allow HTTP/S traffic to our webservers from our specified incoming IP block
resource "aws_security_group" "webserver-sg"{
    name = "${var.user}_${var.webserver_sg_name}"
    description = "Allow traffic to common webserver ports"
    vpc_id = data.aws_vpc.darkwolf_east_vpc.id
    ingress{
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = var.incoming_ip
    }
    ingress{
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = var.incoming_ip
    }
}

# Allow HTTP/S traffic to our load-balancer from our specified incoming IP block, and out to our webservers
resource "aws_security_group" "load-balancer-sg"{
    name = "${var.user}_${var.load_balancer_sg_name}"
    description = "Allow traffic to and from load balancer"
    vpc_id = data.aws_vpc.darkwolf_east_vpc.id
    ingress{
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = var.incoming_ip
    }
    ingress{
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = var.incoming_ip
    }
    egress{
      from_port = 80
      to_port = 80
      protocol = "tcp"
      security_groups = [aws_security_group.webserver-sg.id]
    }
}

# Allow HTTP/S traffic from our load balancer to our webservers
# There is a cyclic dependency between our webservers and lb for this security group
# The next two steps allow us to add an additional rule to an existing group
resource "aws_security_group_rule" "lb-to-webserver-http"{
  security_group_id = aws_security_group.webserver-sg.id
  from_port = 80
  to_port = 80
  protocol = "tcp"
  type = "ingress"
  source_security_group_id = aws_security_group.load-balancer-sg.id
}

resource "aws_security_group_rule" "lb-to-webserver-https"{
  security_group_id = aws_security_group.webserver-sg.id
  from_port = 443
  to_port = 443
  protocol = "tcp"
  type = "ingress"
  source_security_group_id = aws_security_group.load-balancer-sg.id
}

# Allow traffic from our defined block to our database server
resource "aws_security_group" "database-sg"{
    name = "${var.user}_${var.database_sg_name}"
    description = "Allow traffic to common database ports"
    vpc_id = data.aws_vpc.darkwolf_east_vpc.id
    ingress{
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = var.incoming_ip
    }
}
# Allow traffic from our webservers to our database
resource "aws_security_group_rule" "webserver-to-database"{
  security_group_id = aws_security_group.database-sg.id
  from_port = 5432
  to_port = 5432
  protocol = "tcp"
  type = "ingress"
  source_security_group_id = aws_security_group.webserver-sg.id
}