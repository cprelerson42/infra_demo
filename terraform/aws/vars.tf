variable "num_webservers" {
  type = number
  default = 1
}
variable "user"{
    type = string
    default = ""
}

variable "ami_name" {
    type = string
    default = "amzn2-ami-hvm-2.0.*.0-x86_64-gp2"
}

variable "vpc_name"{
    type = string
    default = "darkwolf-east"
}

variable "webserver_subnet_name"{
    type = string
    default = "default"
}

variable "default_sg_name" {
    default = "default"
}

variable "webserver_sg_name" {
    default = "web-server"
}
variable "load_balancer_sg_name" {
    default = "lb"
}

variable "database_sg_name" {
    default = "database-server"
}

variable "incoming_ip" {}
