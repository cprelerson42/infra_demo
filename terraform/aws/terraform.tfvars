# Values defined here will over ride the values defined in vars.tf
# https://www.terraform.io/docs/configuration/variables.html#variable-definition-precedence
num_webservers = 2
user = "prelerson"
incoming_ip = ["98.175.198.60/32"]