# Create ~/.aws/credentials file
# [default]
# aws_access_key_id =
# aws_secret_access_key =

provider "aws" {
    region = "us-gov-east-1"
}

# Create webservers. Count variable is Terraform's method for looping
resource "aws_instance" "webserver" {
  count = var.num_webservers
  ami           = data.aws_ami_ids.amazon_linux_2.ids[0]
  instance_type = "t3.small"
  key_name      = "ansible"
  tags = {
    schedule = "True",
    Name = "${var.user}_webserver_${count.index}"
  }
  subnet_id              = tolist(data.aws_subnet_ids.subnets.ids)[0]
  vpc_security_group_ids = [data.aws_security_groups.default-sg.ids[0], aws_security_group.webserver-sg.id]
}

# Create database server
resource "aws_instance" "database-server" {
  ami           = data.aws_ami_ids.amazon_linux_2.ids[0]
  instance_type = "t3.small"
  key_name      = "ansible"
  tags = {
    schedule = "True",
    Name = "${var.user}_database"
  }
  subnet_id              = tolist(data.aws_subnet_ids.subnets.ids)[0]
  vpc_security_group_ids = [data.aws_security_groups.default-sg.ids[0], aws_security_group.database-sg.id]
}
