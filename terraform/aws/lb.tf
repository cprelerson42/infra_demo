# Create the application load balancer
# https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/load-balancer-getting-started.html
resource "aws_lb" "webserver-lb"{
  name = "${var.user}-webserver-lb"
  internal = false
  load_balancer_type = "application"
  subnets = tolist(data.aws_subnet_ids.all-subnets.ids)
  security_groups = [data.aws_security_groups.default-sg.ids[0], aws_security_group.load-balancer-sg.id]
}

# Create the target group that our lb will forward traffic to
# We're using the instance-type target, so we feed it our webservers' instance-ids
resource "aws_lb_target_group" "webservers-tg"{
  name = "${var.user}-webserver-tg"
  port = 80
  protocol = "HTTP"
  vpc_id = data.aws_vpc.darkwolf_east_vpc.id
}

# Attach a listener to our lb that will listen for traffic on port 80 and forward to our target group
resource "aws_lb_listener" "lb-http"{
  load_balancer_arn = aws_lb.webserver-lb.arn
  port = "80"
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.webservers-tg.arn
  }
}

# Attach our web server instances to our target group
resource "aws_lb_target_group_attachment" "webservers-tg-attach"{
  count = var.num_webservers
  target_group_arn = aws_lb_target_group.webservers-tg.arn
  target_id = aws_instance.webserver[count.index].id
  port = 80
}
