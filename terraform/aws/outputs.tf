# Print out useful info about our created resources at the end of an 'apply'
output "web_server_instance_id" {
  value = aws_instance.webserver[*].id
}
output "web_server_instance_ip_addr" {
  value = aws_instance.webserver[*].public_ip
}
output "web_server_private_ip_addr" {
  value = aws_instance.webserver[*].private_ip
}
output "web_server_names" {
  value = aws_instance.webserver[*].tags.Name
}


output "db_server_instance_id" {
  value = aws_instance.database-server.id
}
output "db_server_instance_ip_addr" {
  value = aws_instance.database-server.public_ip
}
output "db_server_private_ip_addr" {
  value = aws_instance.database-server.private_ip
}
output "db_server_names" {
  value = aws_instance.database-server.tags.Name
}
