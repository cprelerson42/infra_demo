# Get Amazon Linux 2 AMI ID
data "aws_ami_ids" "amazon_linux_2"{
    owners = ["045324592363"]  # owner ID of amazon linux
    filter {
        name = "name"
        values = [var.ami_name]
    }
}

# Get VPC ID
data "aws_vpc" "darkwolf_east_vpc" {
        tags= {
                Name = var.vpc_name
        }
}

# Get subnet ID for desired subnet
data "aws_subnet_ids" "subnets"{
        vpc_id = data.aws_vpc.darkwolf_east_vpc.id
        filter{
            name = "tag:Name"
            values = [var.webserver_subnet_name]
        }
}

# Get subnet IDs for all subnets
# Get subnet IDs
data "aws_subnet_ids" "all-subnets"{
        vpc_id = data.aws_vpc.darkwolf_east_vpc.id
}


# # Get Security group IDs
data "aws_security_groups" "default-sg"{
    filter {
        name = "group-name"
        values = [var.default_sg_name]
    }

    filter {
        name = "vpc-id"
        values = [data.aws_vpc.darkwolf_east_vpc.id]
    }
}
