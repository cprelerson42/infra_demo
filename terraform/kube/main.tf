provider "kubernetes" {
  config_context_cluster = "gke_infra-demo-265315_us-central1-a_tf-kube-cluster"
}

# Create our namespace that will house all of our resources
resource "kubernetes_namespace" "web-demo" {
  metadata {
    annotations = {
      name = var.namespace
    }
    name = var.namespace
  }
}

# Random number values used for our ports to avoid collision between multiple pods running on the same cluster
resource "random_integer" "mongo_port" {
  min = 10000
  max = 50000
}

resource "random_integer" "redis_port" {
  min = 10000
  max = 50000
}

output "mongo_port" {
  value = random_integer.mongo_port.result
}

output "redis_port" {
  value = random_integer.redis_port.result
}

output "mongo_url"{
  value =  kubernetes_service.mongo-svc.load_balancer_ingress[0].ip
}

output "redis_url"{
  value =  kubernetes_service.redis-svc.load_balancer_ingress[0].ip
}
