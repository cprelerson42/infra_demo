# The various bits of config below very closely match the k8s API
# https://kubernetes.io/docs/concepts/overview/kubernetes-api/
# and the definition files (typically yaml or json) used to create resources
# For this project I created everything through the GKE GUI, downloaded the YAML file 
# that was created, then broke it up in the various TF resources
# Terraform is using the k8s API, everything done here can be done with the kubectl tool
# Therefore, this config should work on GKE, AKS, PKS, OpenShift, or any other CCNF-compliant k8s distro

resource "kubernetes_persistent_volume_claim" "redis-vol" {
  metadata {
    name = "redis-vol"
    namespace = kubernetes_namespace.web-demo.id
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "10Gi"
      }
    }
    storage_class_name = "standard"
  }
}

resource "kubernetes_deployment" "redis-db" {
  metadata {
    name      = var.redis_app_name
    namespace = kubernetes_namespace.web-demo.id
    labels = {
      app = var.redis_app_name
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.redis_app_name
      }
    }
    template {

      metadata {
        labels = {
          app = var.redis_app_name
        }
      }
      spec {
        container {
          name  = "redis"
          image = "redis:latest"

         
          port {
            name           = "redis"
            container_port = 6379
            protocol       = "TCP"
          }

          volume_mount {
            name       = "redis-volume"
            mount_path = "/data"
          }
        }
        volume {
          name = "redis-volume"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.redis-vol.metadata[0].name
          }
        }
      }
    }
  }
}
resource "kubernetes_horizontal_pod_autoscaler" "redis-autoscaler" {
  metadata {
    name = "redis-autoscaler"
    namespace = kubernetes_namespace.web-demo.id
    labels = {
      app = var.redis_app_name
    }
  }
  spec {
    max_replicas = 3
    min_replicas = 1
    scale_target_ref {
      kind = "Deployment"
      name = var.redis_app_name
    }
    target_cpu_utilization_percentage = 80
  }
}

resource "kubernetes_service" "redis-svc" {
  metadata {
    name = "redis-svc"
    namespace = kubernetes_namespace.web-demo.id
    labels = {
      app = var.redis_app_name
    }
  }
  spec {
    selector = {
      app = var.redis_app_name
    }
    session_affinity = "ClientIP"
    port {
      port        = random_integer.redis_port.result
      target_port = 6379
    }
    type = "LoadBalancer"
  }
}