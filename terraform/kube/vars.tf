variable "user"{
    type = string
    default = ""
}

variable "namespace" {
    type = string
    default = "web-demo"
}

variable "mongo_app_name"{
    type = string
    default = "mongo-db"
}

variable "mongo_username" {
    type = string
    default = "mongo"
}

variable "mongo_password" {
    type = string
    default = "mongo"
}

variable "redis_app_name"{
    type = string
    default = "redis"
}
