# Terraform will automatically include any file named 'terraform.tfvars' or '*.auto.tfvars' as input variables. To manually include any other file, 
# you can use '-var-file=/path/to/file'

user = "prelerson"
mongo_username = "web_user"
namespace = "prelerson-web-demo"
mongo_app_name = "prelerson-mongo-db"
redis_app_name = "prelerson-redis"