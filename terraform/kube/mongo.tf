# The various bits of config below very closely match the k8s API
# https://kubernetes.io/docs/concepts/overview/kubernetes-api/
# and the definition files (typically yaml or json) used to create resources
# For this project I created everything through the GKE GUI, downloaded the YAML file 
# that was created, then broke it up in the various TF resources
# Terraform is using the k8s API, everything done here can be done with the kubectl tool
# Therefore, this config should work on GKE, AKS, PKS, OpenShift, or any other CCNF-compliant k8s distro

resource "kubernetes_config_map" "mongo-config-map" {
  metadata {
    name      = "mongo-config"
    namespace = kubernetes_namespace.web-demo.id
  }

  data = {
    MONGO_INITDB_ROOT_USERNAME = var.mongo_username
    MONGO_INITDB_ROOT_PASSWORD = var.mongo_password
  }
}

resource "kubernetes_persistent_volume_claim" "mongo-db-vol" {
  metadata {
    name = "mongo-db-vol"
    namespace = kubernetes_namespace.web-demo.id
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "10Gi"
      }
    }
    storage_class_name = "standard"
  }
}

resource "kubernetes_deployment" "mongo-db" {
  metadata {
    name      = var.mongo_app_name
    namespace = kubernetes_namespace.web-demo.id
    labels = {
      app = var.mongo_app_name
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.mongo_app_name
      }
    }
    template {

      metadata {
        labels = {
          app = var.mongo_app_name
        }
      }
      spec {
        container {
          name  = "mongo"
          image = "mongo:latest"

          env {
            name = "MONGO_INITDB_ROOT_USERNAME"
            value_from {
              config_map_key_ref {
                key  = "MONGO_INITDB_ROOT_USERNAME"
                name = "mongo-config"
              }
            }
          }
          env {
            name = "MONGO_INITDB_ROOT_PASSWORD"
            value_from {
              config_map_key_ref {
                key  = "MONGO_INITDB_ROOT_PASSWORD"
                name = "mongo-config"
              }
            }
          }
          port {
            name           = "mongo"
            container_port = 27017
            protocol       = "TCP"
          }

          volume_mount {
            name       = "mongo-db-volume"
            mount_path = "/data/db/"
          }
        }
        volume {
          name = "mongo-db-volume"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.mongo-db-vol.metadata[0].name
          }
        }
      }
    }
  }
}
resource "kubernetes_horizontal_pod_autoscaler" "mongo-autoscaler" {
  metadata {
    name = "mongo-autoscaler"
    namespace = kubernetes_namespace.web-demo.id
    labels = {
      app = var.mongo_app_name
    }
  }
  spec {
    max_replicas = 3
    min_replicas = 1
    scale_target_ref {
      kind = "Deployment"
      name = var.mongo_app_name
    }
    target_cpu_utilization_percentage = 80
  }
}

resource "kubernetes_service" "mongo-svc" {
  metadata {
    name = "mongo-svc"
    namespace = kubernetes_namespace.web-demo.id
    labels = {
      app = var.mongo_app_name
    }
  }
  spec {
    selector = {
      app = var.mongo_app_name
    }
    session_affinity = "ClientIP"
    port {
      port        = random_integer.mongo_port.result
      target_port = 27017
    }
    type = "LoadBalancer"
  }
}